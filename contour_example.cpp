// STL includes
#include <cmath>
#include <sstream>
#include <string>

// Library includes
#include <opencv2/opencv.hpp>
#include <RtMidi.h>

const std::string original_window_name = "Original";
const std::string threshold_window_name = "Threshold";
const std::string contours_window_name = "Contours";
const std::string control_window_name = "Controls";

int h_min = 150;
int h_max = 179;
int l_min = 150;
int l_max = 255;
int s_min = 200;
int s_max = 255;

int erode_1_size = 2;
int erode_2_size = 15;
int dilate_size = 20;

int blur_size = 3;

int canny_lower_threshold = 100;
int canny_upper_threshold = 300;

double camera_angle = 45.0; // Degrees
double camera_laser_distance = 12.0; // Centimetres
double camera_height = 8; // Centimetres
double camera_fov_angle_vertical = 30; // Degrees
double image_height_pixels = 480;

typedef std::vector<cv::Point> Contour;
typedef std::vector<Contour> ContourSet;

void drawOpenContours(cv::Mat &image, const ContourSet &contours);
ContourSet findMeanContours(ContourSet &contours);
cv::Mat preprocess(const cv::Mat &image);
void createWindows();
void createTrackbars();
double calculateAverageHeight(ContourSet input);
void heightToMIDI(double height);

RtMidiOut midiout;

int main(int argc, char *argv[])
{
	if(argc != 2)
	{
		std::cout << "Usage: " << argv[0] << " camera_index" << std::endl;
		return -1;
	}

	std::istringstream to_int(argv[1]);
	int camera_index;
	to_int >> camera_index;

	cv::VideoCapture camera(camera_index);
	if(!camera.isOpened())
	{
		std::cout << "Error opening camera." << std::endl;
		return -1;
	}

	createWindows();
	createTrackbars();

	cv::Mat frame;
	ContourSet contours, mean_contours;
	std::vector<cv::Vec4i> hierarchy;
	double average_height;

	camera >> frame;
	image_height_pixels = frame.size().height;

	char key = '\0';
	while(key != 'q')
	{
		camera >> frame;
		cv::Mat frame_edges = preprocess(frame);

		cv::findContours(frame_edges, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE, cv::Point(0, 0));
		mean_contours = findMeanContours(contours);

		cv::Mat mean_contours_display = cv::Mat::zeros(frame_edges.size(), CV_8UC1);
		drawOpenContours(mean_contours_display, mean_contours);

		//static cv::VideoWriter raw_writer("raw_output.avi", CV_FOURCC('M','J','P','G'), 30, frame.size(), true);
		//static cv::VideoWriter contours_writer("contours_output.avi", CV_FOURCC('M','J','P','G'), 30, mean_contours_display.size(), false);

		//raw_writer << frame;
		//contours_writer << mean_contours_display;

		if(mean_contours.size() > 0)
		{
			average_height = calculateAverageHeight(mean_contours);
			heightToMIDI(average_height);
			std::cout << "Average height (cm): " << average_height << std::endl;
		}

		cv::imshow(original_window_name, frame);
		cv::imshow(contours_window_name, mean_contours_display);
		key = cv::waitKey(1);
	}

	std::vector<unsigned char> all_notes_off = {176, 120, 0};
	midiout.sendMessage(&all_notes_off);
};

void drawOpenContours(cv::Mat &image, const ContourSet &contours)
{
	for(auto contour : contours)
	{
		for(auto point : contour)
		{
			cv::circle(image, point, 1, 255);
		}
	}
}

ContourSet findMeanContours(ContourSet &contours)
{
	ContourSet mean_contours;
	// For each contour, find the mean vertical point for each image column
	for(auto contour : contours)
	{
		// First, sort the vector by columns then rows
		std::sort(contour.begin(), contour.end(),
			[] (const cv::Point &i, const cv::Point &j)
			{
				return (i.x < j.x) || ((i.x == j.x) && (i.y < j.y));
			}
		);

		// Next, take the first and last points of each column and find their midpoint, storing these in a new vector
		mean_contours.emplace_back();
		auto point = contour.begin();
		while(point != contour.end())
		{
			int current_column = point->x;
			int top_row = point->y;
			int bottom_row;

			// Iterate over the points in the current column
			while(point != contour.end() && point->x == current_column)
			{
				bottom_row = point->y;
				point++;
			}

			// Now, find the midpoint between top_row and bottom_row
			double midpoint = ((double)top_row + (double)bottom_row)/2.0;
			int midpoint_pixel = std::round(midpoint);

			// Finally, create a new point with current_column & midpoint_pixel as coordinates
			mean_contours.back().emplace_back(current_column, midpoint_pixel);
		}
	}
	return mean_contours;
}

cv::Mat preprocess(const cv::Mat &image)
{
	cv::Mat frame_hls, frame_thresholded, frame_edges;
	cv::cvtColor(image, frame_hls, CV_BGR2HLS);
	cv::inRange(frame_hls, cv::Scalar(h_min, l_min, s_min), cv::Scalar(h_max, l_max, s_max), frame_thresholded);
	cv::erode(frame_thresholded, frame_thresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(erode_1_size, erode_1_size)));
	cv::dilate(frame_thresholded, frame_thresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(dilate_size, dilate_size)));
	cv::erode(frame_thresholded, frame_thresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(erode_2_size, erode_2_size)));
	cv::blur(frame_thresholded, frame_thresholded, cv::Size(blur_size, blur_size));
	cv::Canny(frame_thresholded, frame_edges, canny_lower_threshold, canny_upper_threshold, 3);
	cv::imshow(threshold_window_name, frame_thresholded);

	//static cv::VideoWriter threshold_writer("threshold_output.avi", CV_FOURCC('M','J','P','G'), 30, frame_thresholded.size(), false);
	//threshold_writer << frame_thresholded;

	return frame_edges;
}

void createWindows()
{
	cv::namedWindow(original_window_name, cv::WINDOW_AUTOSIZE);
	cv::namedWindow(threshold_window_name, cv::WINDOW_AUTOSIZE);
	cv::namedWindow(contours_window_name, cv::WINDOW_AUTOSIZE);
	cv::namedWindow(control_window_name, cv::WINDOW_AUTOSIZE);
}

void createTrackbars()
{
	cv::createTrackbar("Hue min", control_window_name, &h_min, 179);
	cv::createTrackbar("Hue max", control_window_name, &h_max, 179);
	cv::createTrackbar("Luminance min", control_window_name, &l_min, 255);
	cv::createTrackbar("Luminance max", control_window_name, &l_max, 255);
	cv::createTrackbar("Saturation min", control_window_name, &s_min, 255);
	cv::createTrackbar("Saturation max", control_window_name, &s_max, 255);
	cv::createTrackbar("Erode 1 size", control_window_name, &erode_1_size, 20);
	cv::createTrackbar("Dilate size", control_window_name, &dilate_size, 20);
	cv::createTrackbar("Erode 2 size", control_window_name, &erode_2_size, 20);
	cv::createTrackbar("Blur size", control_window_name, &blur_size, 20);
	cv::createTrackbar("Canny lower threshold", control_window_name, &canny_lower_threshold, 1000);
	cv::createTrackbar("Canny upper threshold", control_window_name, &canny_upper_threshold, 1000);
}

double calculateAverageHeight(ContourSet input)
{
	double average_height = 0;
	double point_count = 0;

	double image_angle = 0;
	double total_angle = 0;
	double total_angle_radians = 0;
	double actual_height = 0;

	const static double camera_top_angle = camera_angle + (0.5*camera_fov_angle_vertical);
	const static double image_angle_multiplier = (camera_fov_angle_vertical / image_height_pixels);
	for(auto contour : input)
	{
		for(auto point : contour)
		{
			image_angle = ((double) point.y) * image_angle_multiplier;
			total_angle = camera_top_angle - image_angle;
			total_angle_radians = M_PI*(total_angle/180);
			actual_height = camera_height + (camera_laser_distance * tan(total_angle_radians));
			average_height += actual_height;
			point_count++;
		}
	}

	average_height /= point_count;
	return average_height;
}

void heightToMIDI(double height)
{
	static bool is_initialised = false;
	static unsigned char prev_pitch = 0;
	unsigned char pitch = 60 + std::round(height*2);

	if(!is_initialised)
	{
		midiout.openPort(0);
		std::vector<unsigned char> note_on = {128, pitch, 30};
		std::cout << "Note on " << (int)pitch << std::endl;
		midiout.sendMessage(&note_on);
		prev_pitch = pitch;
		is_initialised = true;
	}

	else
	{
		if(pitch != prev_pitch)
		{
			std::vector<unsigned char> note_off;
			note_off.push_back(128);
			note_off.push_back(prev_pitch);
			note_off.push_back(30);

			std::vector<unsigned char> note_on;
			note_on.push_back(144);
			note_on.push_back(pitch);
			note_on.push_back(30);

			double control_value = (pitch - 70) * (126)/(120-70);

			std::vector<unsigned char> control_change;
			control_change.push_back(177);
			control_change.push_back(1);
			control_change.push_back((unsigned char)control_value);

			midiout.sendMessage(&control_change);
			midiout.sendMessage(&note_off);
			midiout.sendMessage(&note_on);

			prev_pitch = pitch;
		}
	}
}
