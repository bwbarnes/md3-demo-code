// STL includes
#include <string>

// Library includes
#include <opencv2/opencv.hpp>

const int hue_min = 240;
const int hue_max = 16;
const int sat_min = 0;
const int sat_max = 255;
const int val_min = 64;
const int val_max = 255;

std::string window_name = "Output";
std::string control_window_name = "Controls";
int main(int argc, char *argv[])
{
	cv::VideoCapture camera(1);
	cv::namedWindow(window_name, cv::WINDOW_AUTOSIZE);
	cv::namedWindow(control_window_name, cv::WINDOW_AUTOSIZE);

	int threshold = 128;
	cv::createTrackbar("Threshold", control_window_name, &threshold, 255);

	char key = '\0';
	cv::Mat frame, frame_grey, frame_threshold, frame_output;
	while(key != 'q')
	{
		camera >> frame;
		cv::cvtColor(frame, frame_grey, CV_BGR2GRAY);
		cv::threshold(frame_grey, frame_threshold, threshold, 255, cv::THRESH_BINARY);
		cv::imshow(window_name, frame);
		key = cv::waitKey(1);
	}

	return 0;
}

