// STL includes
#include <string>

// Library includes
#include <opencv2/opencv.hpp>

const int hue_min = 240;
const int hue_max = 16;
const int sat_min = 0;
const int sat_max = 255;
const int val_min = 64;
const int val_max = 255;

std::string window_name = "Output";
int main(int argc, char *argv[])
{
	cv::VideoCapture camera(1);
	cv::namedWindow(window_name, cv::WINDOW_AUTOSIZE);

	char key = '\0';
	cv::Mat frame, frame_hsv, frame_threshold, frame_output;
	while(key != 'q')
	{
		camera >> frame;
		cv::cvtColor(frame, frame_hsv, CV_BGR2HSV);
		cv::inRange(frame_hsv, cv::Scalar(hue_min, sat_min, val_min), cv::Scalar(hue_max, sat_max, val_max), frame_threshold);
		//cv::cvtColor(frame_threshold, frame_output, CV_HSV2BGR); 
		cv::imshow(window_name, frame_threshold);
		key = cv::waitKey(1);
	}

	return 0;
}

